# React Components Demo

This is the demonstration site for 
[@jhohman/react-components](https://bitbucket.org/hohmanjl/react-components/overview)

Demo http://jhohman-react-components.surge.sh/

## Getting Started

First install the dependencies.

```
$ npm install
```

Then run the server.

```
$ npm start
```

## Publish

Install `surge`.

```
$ npm install -g surge
```

Deploy.

```
$ surge --domain my-domain.surge.sh build/
```
