import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux';

import DemoSection from '../DemoSection';

import { Input, inputReducer } from '@jhohman/react-components/Input.min';
import { Loader } from '@jhohman/react-components/Loader.min';

import {
  loaderSignature,
  loaderSampleCode
} from './constants';

import '@jhohman/react-components/css/Loader.min.css';

const reducers = combineReducers({
  input: inputReducer,
});

const store = createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__
  && window.__REDUX_DEVTOOLS_EXTENSION__()
);


class LoaderFragment extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      message: 'Loading...',
      show: true,
      size: 50,
      twin: false,
    };
  }

  formatter(val) {
    return val === undefined ? 'undefined' : val;
  };

  inputCallback(property, val) {
    this.setState({ [property]: val });
  }

  render() {
    const { message, show, size, twin } = this.state;

    return (
      <Provider store={store}>
        <div>
          <Loader
            show={show}
            size={size}
            twin={twin}
            message={message} />
          <hr/>

          <div className="alert alert-warning" role="alert">
            <span className="glyphicon glyphicon-alert" />
            {' '}
            Store interface not implemented for this
            component yet.
          </div>

          <div>
            <form className="form-horizontal">
              <div className="form-group">
                <label
                  htmlFor="inputLoaderMessage"
                  className="col-xs-12 col-sm-4 control-label">
                  Message
                </label>
                <div className="col-xs-12 col-sm-8">
                  <Input
                    name="inputLoaderMessage"
                    className="form-control"
                    id="inputLoaderMessage"
                    inputCallback={this.inputCallback.bind(this, 'message')}
                    value={message}
                    placeholder="Input field" />
                </div>
              </div>
              <div className="form-group">
                <label
                  htmlFor="inputLoaderSize"
                  className="col-xs-12 col-sm-4 control-label">
                  Size
                </label>
                <div className="col-xs-12 col-sm-8">
                  <Input
                    name="inputLoaderSize"
                    className="form-control"
                    id="inputLoaderSize"
                    inputCallback={this.inputCallback.bind(this, 'size')}
                    value={size}
                    type="number"
                    placeholder="Input field" />
                </div>
              </div>
              <div className="form-group">
                <label
                  htmlFor="inputLoaderTwin"
                  className="col-xs-12 col-sm-4 control-label">
                  Twin
                </label>
                <div className="col-xs-12 col-sm-8">
                  <Input
                    name="inputLoaderTwin"
                    className="form-control"
                    id="inputLoaderTwin"
                    inputCallback={this.inputCallback.bind(this, 'twin')}
                    value={twin}
                    type="checkbox" />
                </div>
              </div>
              <div className="form-group">
                <label
                  htmlFor="inputLoaderShow"
                  className="col-xs-12 col-sm-4 control-label">
                  Show
                </label>
                <div className="col-xs-12 col-sm-8">
                  <Input
                    name="inputLoaderShow"
                    className="form-control"
                    id="inputLoaderShow"
                    inputCallback={this.inputCallback.bind(this, 'show')}
                    value={show}
                    type="checkbox" />
                </div>
              </div>
            </form>
          </div>
        </div>
      </Provider>
    );
  }
}

function LoaderDemo(props) {
  return (
    <DemoSection
      { ...props }
      demo={<LoaderFragment />}
      name="Loader"
      sampleCode={loaderSampleCode}
      signature={loaderSignature} />
  );
}

LoaderDemo.propTypes = {
  codeStyle: PropTypes.object
};

export default LoaderDemo;
