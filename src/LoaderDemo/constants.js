const loaderSignature = `<Loader
  className={string|optional}
  id={string|optional}
  message={string|optional}
  show={bool|required}
  size={number|default:LARGE|optional}
  twin={bool|default:false|optional} />`;

const loaderExampleTag = `<Loader
  show={true}
  message="Loading..."
  size="50" />`;

const loaderSampleCode = `import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import { Loader } from '@jhohman/react-components/Loader.min';

render(
  ${loaderExampleTag},
  document.body
);
`;

export {
  loaderSignature,
  loaderExampleTag,
  loaderSampleCode,
};
