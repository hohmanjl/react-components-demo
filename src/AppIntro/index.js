import React from 'react';
import pkg from '@jhohman/react-components/package.json';


function AppIntro() {
  return (
    <div>
      <p className="App-intro">
        React UI Components v{pkg.version}
        <span className="no-wrap">
          {' '}by{' '}
          <a
            href="https://bitbucket.org/hohmanjl/"
            aria-label="hohmanjl on bitbucket">
          James Hohman
        </a>
        </span>
      </p>
      <p>
        Repository {' '}
        <a
          href="https://bitbucket.org/hohmanjl/react-components"
          aria-label="Repository URL to hohmanjl react components" >
          https://bitbucket.org/hohmanjl/react-components
        </a>
      </p>
      <p>
        NPM Registry {' '}
        <a
          href="https://www.npmjs.com/package/@jhohman/react-components"
          aria-label="@jhohman/react-components at npm registry">
          https://www.npmjs.com/package/@jhohman/react-components
        </a>
      </p>
      <p>
        Installation {' '}
        <code>npm install --save @jhohman/react-components</code>
      </p>
    </div>
  );
}

export default AppIntro;
