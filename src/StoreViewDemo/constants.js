const storeViewSignature = `<StoreView
  className={string|optional}
  id={string|optional}
  formatter={function|default:val => val}
  storePath={string|required} />`;

const storeViewExampleTag = `<Provider store={store}>
    <StoreView storePath="myStore.widget" />
  </Provider>`;

const storeViewSampleCode = `import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import StoreView from '@jhohman/react-components/StoreView.min';

render(
  ${storeViewExampleTag},
  document.body
);
`;

export {
  storeViewSignature,
  storeViewExampleTag,
  storeViewSampleCode,
};
