import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import DemoSection from '../DemoSection';

import { Input, inputReducer } from '@jhohman/react-components/Input.min';
import StoreView from '@jhohman/react-components/StoreView.min';

import {
  storeViewSignature,
  storeViewSampleCode
} from './constants';

const store = createStore(
  inputReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__
  && window.__REDUX_DEVTOOLS_EXTENSION__()
);


class StoreViewFragment extends React.PureComponent {
  formatter(val) {
    return val === undefined ? 'undefined' : val;
  };

  render() {
    return (
      <Provider store={store}>
        <div>
          <StoreView
            formatter={this.formatter}
            storePath="inputStore" />
          <hr/>

          <div>
            <form className="form-horizontal">
              <div className="form-group">
                <label
                  htmlFor="inputStore"
                  className="col-xs-12 col-sm-4 control-label">
                  Store
                </label>
                <div className="col-xs-12 col-sm-8">
                  <Input
                    name="inputStore"
                    className="form-control"
                    id="inputStore"
                    placeholder="Store value" />
                </div>
              </div>
            </form>
          </div>
        </div>
      </Provider>
    );
  }
}

function StoreViewDemo(props) {
  return (
    <DemoSection
      { ...props }
      demo={<StoreViewFragment />}
      name="StoreView"
      sampleCode={storeViewSampleCode}
      signature={storeViewSignature} />
  );
}

StoreViewDemo.propTypes = {
  codeStyle: PropTypes.object
};

export default StoreViewDemo;
