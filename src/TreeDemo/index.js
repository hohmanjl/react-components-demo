import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux';

import DemoSection from '../DemoSection';

import { Input, inputReducer } from '@jhohman/react-components/Input.min';
import { Select, selectReducer } from '@jhohman/react-components/Select.min';
import { StoreView } from '@jhohman/react-components/StoreView.min';
import {
  Tree,
  treeReducer,
} from '@jhohman/react-components/Tree.min';

import {
  callbackSignatures,
  tree,
  treeSignature,
  treeSampleCode
} from './constants';

import '@jhohman/react-components/css/Tree.min.css';
import './tree.css';

import {
  defaultSortOption,
  iContainsFilter,
  sortOptions,
  treeCallback
} from './utils';

const reducers = combineReducers({
  input: inputReducer,
  select: selectReducer,
  tree: treeReducer,
});

const parentReducer = combineReducers({
  rootStore: reducers
});

const store = createStore(
  parentReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__
  && window.__REDUX_DEVTOOLS_EXTENSION__()
);


class TreeFragment extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      sortFn: defaultSortOption.fn,
      filterFn: undefined,
    };

    this.inputCallback = this.inputCallback.bind(this);
    this.selectCallback = this.selectCallback.bind(this);
  }

  formatter(val) {
    if (val === undefined) {
      return 'undefined';
    }

    if (val === null) {
      return val;
    }

    if (!val) {
      return {};
    }

    return val;
  };

  inputCallback(value) {
    if (value) {
      return this.setState({ filterFn: iContainsFilter.bind(null, value) });
    }

    this.setState({ filterFn: undefined });
  }

  selectCallback(value) {
    const selected = sortOptions.filter((option) => {
      return option.value === value;
    });

    this.setState({ sortFn: selected[0].fn });
  }

  render() {
    const { filterFn, sortFn } = this.state;

    return (
      <Provider store={store}>
        <div>
          <Tree
            className="my-class"
            filter={filterFn}
            name="myTree"
            selectCallback={treeCallback}
            sort={sortFn}
            storePath="rootStore.tree"
            tree={tree} />
          <hr/>

          <div>
            <form className="form-horizontal">
              <div className="form-group">
                <label
                  htmlFor="inputSort"
                  className="col-xs-12 col-sm-4 control-label">
                  Sort
                </label>
                <div className="col-xs-12 col-sm-8">
                  <Select
                    className="form-control"
                    id="inputSort"
                    name="sort"
                    options={sortOptions}
                    selectCallback={this.selectCallback}
                    selected={defaultSortOption.value}
                    storePath="rootStore.select" />
                </div>
              </div>
              <div className="form-group">
                <label
                  htmlFor="inputFilter"
                  className="col-xs-12 col-sm-4 control-label">
                  Filter
                </label>
                <div className="col-xs-12 col-sm-8">
                  <Input
                    name="inputFilter"
                    className="form-control"
                    id="inputFilter"
                    inputCallback={this.inputCallback}
                    placeholder="iContains" />
                </div>
              </div>
              <div className="form-group">
                <label
                  className="col-xs-12 col-sm-4 control-label">
                  Selected
                </label>
                <div className="col-xs-12 col-sm-8">
                  <StoreView
                    formatter={this.formatter}
                    storePath="rootStore.tree.myTree.selected" />
                </div>
              </div>
            </form>
          </div>
        </div>
      </Provider>
    );
  }
}

function TreeDemo(props) {
  return (
    <DemoSection
      { ...props }
      callbackSignatures={callbackSignatures}
      demo={<TreeFragment />}
      name="Tree"
      sampleCode={treeSampleCode}
      signature={treeSignature} />
  );
}

TreeDemo.propTypes = {
  codeStyle: PropTypes.object
};

export default TreeDemo;
