const usa = {
  id: 'usa',
  label: 'United States of America',
  children: [
    {
      id: 'ny',
      label: 'New York',
      children: [
        {
          id: 'nyc',
          label: 'New York City',
          children: [
            {
              id: 'queens-county',
              label: 'Queens County',
            },
            {
              id: 'new-york-county',
              label: 'New York County',
            },
            {
              id: 'kings-county',
              label: 'Kings County',
            },
          ],
        },
        {
          id: 'syracuse',
          label: 'Syracuse',
        },
        {
          id: 'albany',
          label: 'Albany',
          children: [
            {
              id: 'albany-county',
              label: 'Albany County',
            },
          ],
        },
      ],
    },
    {
      id: 'ga',
      label: 'Georgia',
      children: [
        {
          id: 'atl',
          label: 'Atlanta',
          children: [
            {
              id: 'fulton-county',
              label: 'Fulton County',
            },
            {
              id: 'forsyth-county',
              label: 'Forsyth County',
            },
          ],
        },
      ],
    },
    {
      id: 'fl',
      label: 'Florida',
    },
  ],
};

const europe = {
  id: 'europe',
  label: 'Europe',
  children: [
    {
      id: 'fr',
      label: 'France',
      children: [
        {
          id: 'paris',
          label: 'Paris',
        },
        {
          id: 'nice',
          label: 'Nice',
        },
      ],
    },
    {
      id: 'it',
      label: 'Italy',
    },
    {
      id: 'germany',
      label: 'Germany',
      children: [
        {
          id: 'berlin',
          label: 'Berlin',
        },
      ],
    },
  ],
};

const asia = {
  id: 'asia',
  label: 'Asia',
};

const tree = {
  id: 'root',
  label: 'root',
  children: [
    usa,
    asia,
    europe,
  ],
};

const callbackSignatures = [
  {
    label: 'selectCallback',
    description: "Optional callback fired on node selection. "
      + "Note: it only sends the clicked node.",
    signature: '(node|object) => { }'
  }
];


const treeSignature = `<Tree
  className={string|optional}
  id={string|optional}
  filter={function|default:all}
  initialState={TREE_EXPANDED|default:TREE_COLLAPSED|node_path} 
  multiSelectEnabled={boolean|default:true}
  name={string|required}
  parentSelectEnabled={boolean|default:true}
  selectCallback={function|default:noop}
  showSelectIndicator={boolean|default:true}
  sort={function|default:none}
  storePath={string|optional}
  tree={tree|required} />`;

const treeExampleTag = `<Provider store={store}>
    <Tree
      name="myTree"
      selectCallback={selectCallback}
      tree={tree} />
  </Provider>`;

const treeSampleCode = `import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

// Minimum tree styles
import '@jhohman/react-components/css/Tree.min.css';

import { 
  Tree,  // Component
  treeReducer,  // Reducer is always component name + Reducer.
  TREE_EXPANDED,  // You must use this constant for expanded
  TREE_COLLAPSED   // You must use this constant for collapsed
} from '@jhohman/react-components/Tree.min';

const store = createStore(treeReducer);

const tree = {
  id: 'root',
  label: 'root',
  children: [  // Required at root.
    {
      id: 'element1',
      label: ...,
      children: [  // Optional, everywhere else
        {
          id: 'element1.1',
          label: ...,
          children: ...
        }
      ]
    },
    ...
    {
      id: 'elementN',
      ...
    }
  ]
};

render(
  ${treeExampleTag},
  document.body
);
`;

export {
  callbackSignatures,
  tree,
  treeSignature,
  treeExampleTag,
  treeSampleCode,
};
