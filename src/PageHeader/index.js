import React from 'react';

import logo from '../logo.svg';

function PageHeader(props) {
  return (
    <header id={props.id} className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
      <h1 className="App-title">
        React UI Components Demo
      </h1>
    </header>
  );
}

export default PageHeader;
