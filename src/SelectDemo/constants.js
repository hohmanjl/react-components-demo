const defaultSelectOption = {
  value: 'default',
  label: 'default',
};

const selectOptions = [
  {
    value: 'option1',
    label: 'Option 1',
  },
  {
    value: 'option2',
    label: 'Option 2',
  },
  {
    value: 'option...',
    label: '...',
  },
  {
    value: 'optionN',
    label: 'Option n',
  },
  defaultSelectOption,
];

const callbackSignatures = [
  {
    label: 'selectCallback',
    description: 'Optional callback fired when an element is selected.',
    signature: '(optionValue|string|number) => { }'
  }
];

const selectSignature = `<Select
  className={string|optional}
  id={string|optional}
  name={string|required}
  options={arrayOf(Objects(label,value))|required}
  selected={string|number|optional}
  selectCallback={function|default:noop}
  storePath={string|optional} />`;

const selectExampleTag = `<Provider store={store}>
    <Select
      name="mySelect"
      options={selectOptions}
      selected="option1"
      selectCallback={selectCallback} />
  </Provider>`;

const selectSampleCode = `import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import { 
  Select,
  selectReducer,
} from '@jhohman/react-components/Select.min';

const store = createStore(selectReducer);

const selectOptions = ${JSON.stringify(selectOptions, null, 2)}

render(
  ${selectExampleTag},
  document.body
);
`;

export {
  callbackSignatures,
  defaultSelectOption,
  selectOptions,
  selectSignature,
  selectExampleTag,
  selectSampleCode,
};
