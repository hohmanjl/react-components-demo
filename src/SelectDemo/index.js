import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import DemoSection from '../DemoSection';

import { Select, selectReducer } from '@jhohman/react-components/Select.min';
import { StoreView } from '@jhohman/react-components/StoreView.min';

import {
  callbackSignatures,
  defaultSelectOption,
  selectOptions,
  selectSignature,
  selectSampleCode
} from './constants';

const store = createStore(
  selectReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__
  && window.__REDUX_DEVTOOLS_EXTENSION__()
);


class SelectFragment extends React.PureComponent {
  constructor(props) {
    super(props);

    this.selectCallback = this.selectCallback.bind(this);
  }

  formatter(val) {
    return val === undefined ? 'undefined' : val;
  };

  selectCallback(value) {
    console.log(value);
  }

  render() {
    return (
      <Provider store={store}>
        <div>
          <Select
            className="form-control"
            name="selectWidget"
            options={selectOptions}
            selectCallback={this.selectCallback}
            selected={defaultSelectOption.value} />
          <hr/>

          <div>
            <form className="form-horizontal">
              <div className="form-group">
                <label
                  className="col-xs-12 col-sm-4 control-label">
                  Selected
                </label>
                <div className="col-xs-12 col-sm-8">
                  <StoreView
                    formatter={this.formatter}
                    storePath="selectWidget" />
                </div>
              </div>
            </form>
          </div>
        </div>
      </Provider>
    );
  }
}

function SelectDemo(props) {
  return (
    <DemoSection
      { ...props }
      callbackSignatures={callbackSignatures}
      demo={<SelectFragment />}
      name="Select"
      sampleCode={selectSampleCode}
      signature={selectSignature} />
  );
}

SelectDemo.propTypes = {
  codeStyle: PropTypes.object
};

export default SelectDemo;
