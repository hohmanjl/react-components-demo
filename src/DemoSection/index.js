import React from 'react';
import PropTypes from 'prop-types';

import SyntaxHighlighter from 'react-syntax-highlighter';
import { obsidian } from 'react-syntax-highlighter/styles/hljs';
import { TOP_ID } from '../constants';


class DemoSection extends React.PureComponent {
  renderCbDescription(callback) {
    if (!callback.description) {
      return null;
    }

    return <p>{callback.description}</p>;
  }

  renderCbSignatures(callbacks) {
    if (!callbacks) {
      return null;
    }

    const { codeStyle } = this.props;

    return callbacks.map((callback, i) => {
      return (
        <div key={`${callback.label}-${i}`}>
          <h2><code>{callback.label}</code></h2>
          { this.renderCbDescription(callback) }
          <SyntaxHighlighter
            language='javascript'
            style={codeStyle}>
            {callback.signature}
          </SyntaxHighlighter>
        </div>
      );
    });
  }

  renderNavTopLink() {
    const { navTopLink } = this.props;
    if (!navTopLink) {
      return null;
    }

    return (
      <div className="col-xs-12">
        <a className="pull-right"
           href={`#${TOP_ID}`}
           aria-label="back to page top">
          <h4><small>Back to top</small></h4>
        </a>
      </div>
    );
  }

  render() {
    const {
      callbackSignatures,
      codeStyle,
      demo,
      id,
      name,
      signature,
      sampleCode,
    } = this.props;

    return (
      <div className="row" id={id}>
        <div className="col-xs-12">
          <div className="page-header" />
        </div>

        { this.renderNavTopLink() }

        <div className="col-sm-12 col-md-4">
          <h2>{name} Component</h2>
          {demo}
        </div>
        <div className="col-sm-12 col-md-8">
          <h2>JSX Signature</h2>
          <SyntaxHighlighter
            language='javascript'
            style={codeStyle}>
            {signature}
          </SyntaxHighlighter>

          { this.renderCbSignatures(callbackSignatures) }

          <h2>Example usage</h2>
          <SyntaxHighlighter
            language='javascript'
            style={codeStyle}
            showLineNumbers={ true }>
            {sampleCode}
          </SyntaxHighlighter>
        </div>
      </div>
    );
  }
}

DemoSection.propTypes = {
  callbackSignatures: PropTypes.arrayOf(PropTypes.shape({
    description: PropTypes.string,
    label: PropTypes.string.isRequired,
    signature: PropTypes.string.isRequired,
  })),
  codeStyle: PropTypes.object,
  demo: PropTypes.element.isRequired,
  name: PropTypes.string.isRequired,
  navTopLink: PropTypes.bool,
  signature: PropTypes.string.isRequired,
  sampleCode: PropTypes.string.isRequired,
};

DemoSection.defaultProps = {
  codeStyle: obsidian,
  navTopLink: true,
};

export default DemoSection;
