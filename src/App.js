import React, { Component } from 'react';

import { obsidian as codeStyle } from 'react-syntax-highlighter/styles/hljs';

import demoManifest from './demo-manifest.json';
import { TOP_ID } from './constants';

import PageHeader from './PageHeader';
import AppIntro from './AppIntro';
import { Loader, SMALL } from '@jhohman/react-components/Loader.min';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import '@jhohman/react-components/css/Loader.min.css';
import './App.css';
import './ui-theme.css';

class App extends Component {
  constructor(props) {
    super(props);

    const elements = {};

    this.state = {
      elements,
      demosLoaded: 0,
      loaded: {},
    };

    this.loadElements(elements);
  }

  loadElements(elements) {
    const {loaded} = this.state;

    return demoManifest.map((demoName, i) => {
      // fake delay so we can see the pretty loaders
      const delay = 500 * i;
      return import(`./${demoName}`)
        .then(val => {
          setTimeout(() => {
            loaded[demoName] = true;
            elements[demoName] = val.default;
            this.setState({ demosLoaded: this.state.demosLoaded + 1 });
          }, delay);
        });
    });
  }

  renderDemos() {
    const { elements } = this.state;

    return demoManifest.map((demoName, i) => {
      const Demo = elements[demoName];

      if (!Demo) {
        return null;
      }

      const props = {
        navTopLink: i > 0,
        id: demoName,
        codeStyle,
      };

      return <Demo key={demoName} {...props} />;
    });
  }

  renderLoaders() {
    const {loaded} = this.state;

    return demoManifest.map((demoName) => {
      const link = `#${demoName}`;

      return (
        <div key={demoName}>
          {'| '}
          <Loader
            show={!loaded[demoName]}
            size={SMALL}/>
          <a href={link}>
            {
              demoName
                .replace('Demo', '')
                .replace(/([A-Z])/g, ' $1')
            }
          </a>
          {' |'}
        </div>
      );
    });
  }

  render() {
    return (
      <div className="App">
        <PageHeader id={TOP_ID} />
        <AppIntro />
        <div className="container">
          <div className="loaders">
            { this.renderLoaders() }
          </div>
          { this.renderDemos() }
        </div>
      </div>
    );
  }
}

export default App;
