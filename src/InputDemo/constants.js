const callbackSignatures = [
  {
    label: 'inputCallback',
    description: 'Optional, debounced callback fired when on input.',
    signature: '(value|string) => { }'
  }
];

const inputSignature = `<Input
  className={string|optional}
  id={string|optional}
  placeholder={string|optional}
  name={string|required}
  inputCallback={function|default:noop}
  storePath={string|optional} 
  timeout={number|default:100} />`;

const inputExampleTag = `<Provider store={store}>
    <Input
      name="myInput"
      inputCallback={inputCallback}
      placeholder="Enter value" />
  </Provider>`;

const inputSampleCode = `import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import { 
  Input,
  inputReducer,
} from '@jhohman/react-components/Input.min';

const store = createStore(inputReducer);

render(
  ${inputExampleTag},
  document.body
);
`;

export {
  callbackSignatures,
  inputSignature,
  inputExampleTag,
  inputSampleCode,
};
