import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import DemoSection from '../DemoSection';

import { Input, inputReducer } from '@jhohman/react-components/Input.min';
import { StoreView } from '@jhohman/react-components/StoreView.min';

import {
  callbackSignatures,
  inputSignature,
  inputSampleCode
} from './constants';

const store = createStore(
  inputReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__
  && window.__REDUX_DEVTOOLS_EXTENSION__()
);


class InputFragment extends React.PureComponent {
  constructor(props) {
    super(props);

    this.inputCallback = this.inputCallback.bind(this);
  }

  formatter(val) {
    return val === undefined ? 'undefined' : val;
  };

  inputCallback(value) {
    console.log(value);
  }

  render() {
    return (
      <Provider store={store}>
        <div>
          <Input
            name="inputFilter"
            className="form-control"
            id="inputFilter"
            inputCallback={this.inputCallback}
            placeholder="Input field" />
          <hr/>

          <div className="alert alert-info" role="alert">
            <span className="glyphicon glyphicon-info-sign" />
            {' '}
            Store callbacks on this component are debounced
            at a default of 100ms for improved performance.
          </div>

          <div>
            <form className="form-horizontal">
              <div className="form-group">
                <label
                  className="col-xs-12 col-sm-4 control-label">
                  Value
                </label>
                <div className="col-xs-12 col-sm-8">
                  <StoreView
                    formatter={this.formatter}
                    storePath="inputFilter" />
                </div>
              </div>
            </form>
          </div>
        </div>
      </Provider>
    );
  }
}

function InputDemo(props) {
  return (
    <DemoSection
      { ...props }
      callbackSignatures={callbackSignatures}
      demo={<InputFragment />}
      name="Input"
      sampleCode={inputSampleCode}
      signature={inputSignature} />
  );
}

InputDemo.propTypes = {
  codeStyle: PropTypes.object
};

export default InputDemo;
